
/*
mpc version of 
code from  unnamed c program ( book ) 
http://code.mathr.co.uk/book
see mandelbrot_nucleus.c

by Claude Heiland-Allen


gcc -std=c99 m.c -lm -lmpc -lmpfr -lgmp


./a.out 200 "(-1.75 0.003)" 3 100
./a.out 200 "(-1.29441526112226 0.35251517743979)" 10 100

-1.29441526112226e+000	 0.35251517743979e+000

*/


#include <stdio.h>
#include <stdlib.h> // atoi 
#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>
 



 int period ; // period of hyperbolic component of Mandelbrot set 
 int iterMax  ; // maximal number of Newton method steps = iterations 

 // mpc library 
 int bits ; // precision of mpc library 
 int inex; // return value of the function ( not the result of computation )  
 int base = 10; 
 mpc_rnd_t rnd = MPC_RNDNN;
 size_t n_digits = 0;
 mpc_t c0; // initial value of Newton iteration 
 mpc_t center; // last value of Newton iteration  




/*
 mpc_version of :
  mpfr_zero_p
in gmp : Zero is represented by _mp_size and _mp_exp both set to zero, and in that case the _mp_d data is unused
see also FP_ZERO
http://pubs.opengroup.org/onlinepubs/009695399/basedefs/math.h.html

*/
int mpc_zero_p(mpc_t z)
{ int retval= 0; // not true 
  if (mpfr_zero_p(mpc_realref (z)) && mpfr_zero_p(mpc_imagref (z))) 
     retval = 1; // true 
  return retval; 

}

extern int mandelbrot_nucleus(mpc_t c, mpc_t c0,  int period, int iMax) 
{  int retval = 0; //  bad 

   mp_prec_t prec = mpc_get_prec(c0);
   // declare
   mpc_t z;  // 
   mpc_t d;  //
   mpc_t c_new;  //
   mpc_t c_diff;  //
   // init
   mpc_init2(z, prec);
   mpc_init2(d, prec);
   mpc_init2(c_new, prec);
   mpc_init2(c_diff, prec);
   //  set
   mpc_set( c, c0, rnd);

  // newton iterations = steps
  for (int i = 0; i < iMax; ++i) {
   // initial values 
   mpc_set_ui( z, 0, rnd);
   mpc_set_ui( d, 0, rnd);

  // compute zp = f(z=0, p) and dp = df(z=0, p)
    for (int p = 0; p < period; ++p) {
      // d = 2 * z * d + 1;
      mpc_mul(d, d, z, rnd);
      mpc_mul_ui(d, d, 2, rnd);
      mpc_add_ui(d, d, 1, rnd);
     // z = z^2 + c;
     mpc_mul(z, z, z, rnd);
     mpc_add(z, z, c, rnd);
     } //  for (int p ...
    
    // check d == 0
    if (mpc_zero_p(d)) {
       printf ("d = ");
       mpc_out_str (stdout, base, n_digits, d, rnd); //  mpc_out_str (FILE *stream, int base, size_t n_digits, mpc_t op, mpc_rnd_t rnd)
       printf ("\n ");
      retval = 1; // good
      goto done;
    }

   // newton iterations = steps     
    // c_new = c - z / d
    mpc_div(c_new, z, d ,rnd); // z/d
    mpc_sub(c_new, c , c_new, rnd); // c_new = c-z/d 
   // c_diff = c_new - c
    mpc_sub(c_diff, c_new , c, rnd);
    // c = c_new
    mpc_set(c, c_new, rnd);
    // check c_diff = 0
    if (mpc_zero_p(c_diff)) {
      retval = 2; // good 
      goto done;}
 


  } //  for (int i

 

   
   done: ;
   mpc_clear(z);
   mpc_clear(d);
   mpc_clear(c_new);
   mpc_clear(c_diff);


 return retval;
}


 
void DescribeStop(int stop)
{
  switch( stop )
  {
    case 0:
    printf(" method stopped because i = maxiters\n"
    " the result might be close to a nucleus, \n"
    "but rounding errors or insufficient precision \n"
    "mean that the other stopping conditions aren't met.  \n"
    "or it could still be jumping around the fractal \n"
    " basins of attraction and be nowhere close to a nucleus.\n"
    );
    break;
    
    case 1:
    printf(" method stopped because derivative == 0\n");
    break;
    
    //...
    case 2:
    printf(" method stopped because uv = 0\n");
    break;
    
   }
}

void usage(const char *progname) {
  fprintf(stderr,
    "program finds one center ( nucleus) of hyperbolic component of Mandelbrot set using Newton method\n"
    "usage: %s bits center period iterMax\n"
    "outputs space separated complex nucleus on stdout\n"
    "example %s 53 \"(-1.75 0.003)\" 3 100\n"
    "example %s 53 \"(-1.294415112226 0.35251517743979)\" 10 100\n",
    progname, progname, progname);
}






int setup(int argc, char **argv){

// check the input 
 if (argc != 5) { usage(argv[0]); return 1; } // nothing done 
 
 // read  parameters without checking it !
 period = atoi(argv[3]); // period of hyperbolic component of Mandelbrot set 
 iterMax  = atoi(argv[4]); // maximal number of Newton method steps = iterations 

 // mpc library 
 bits = atoi(argv[1]); // precision of mpc library 
 mpc_init2 (c0, bits);
 mpc_init2 (center, bits); 
 inex = mpc_set_str (c0, argv[2], base, rnd); // read z value from string 
 if (inex==-1) {printf(" bad input string \n") ; return 2;} // arguments not read properly
 

 // print input to check it visually 
 //printf ("c0 = ");
 //mpc_out_str (stdout, base, n_digits, c0, rnd); //  mpc_out_str (FILE *stream, int base, size_t n_digits, mpc_t op, mpc_rnd_t rnd)
 //printf ("\n ");

 printf(" setup done \n") ;
 return 0; // all done 
}





 
int main (int argc, char **argv) {


  int iSetupResult;
  int iStopResult; 

  // setup    
  iSetupResult = setup(argc,argv);
  // computations  
  if (iSetupResult == 0) { 
       iStopResult = mandelbrot_nucleus(center, c0,  period, iterMax); 

       // print output to check it visually 
       printf ("center = ");
       mpc_out_str (stdout, base, n_digits, center, rnd); //  mpc_out_str (FILE *stream, int base, size_t n_digits, mpc_t op, mpc_rnd_t rnd)
       printf ("\n ");
       // 
       DescribeStop(iStopResult); 
      }



 // free  
  if (iSetupResult != 1) {
 	mpc_clear(c0);
 	mpc_clear(center);
	}
 // 
 return 0;


}
